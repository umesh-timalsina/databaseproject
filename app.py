import time
import requests
from flask import Flask, render_template, request, jsonify, redirect, jsonify, json, url_for, redirect
import subprocess
from sql_engine import EngineCreator
from flask_sockets import Sockets

app = Flask(__name__)



app.conn_engine = EngineCreator(conn_url='localhost', username='root', password='toor')



@app.route('/', methods=['GET', 'POST'])
def home():
    return render_template('index.html')

@app.route('/mainpage', methods=['GET', 'POST'])
def mainpage():
    databases = app.conn_engine.get_schemas()
    print(databases)
    return render_template('join_page.html', databases=databases)


@app.route('/connect', methods=['POST', 'GET'])
def connect():
    """
        Connect and Return To Join Page
    """
    request_json = request.get_json()
    print(request_json)
    try:
        app.conn_engine = EngineCreator(
            conn_url=request_json['hostName'],
            password=request_json['password'],
            username=request_json['userName'])
    except:
        data = {
            'Msg' : 'Invalid Credentials'
        }
        response = app.response_class(
            response=json.dumps(data),
            status=401,
            mimetype='application/json')
        return response
    return redirect(url_for('mainpage'))

@app.route('/get-tables', methods = ['POST'])
def get_tables():
    """
        A utility method to get tables from a selected database
        For now this method is not needed as tables are hardcoded
    """
    request_json = request.get_json()
    tables = app.conn_engine.select_schema(request_json['selectedTable'])
    data = {
        'tables' : tables
    }
    print(data)
    response = app.response_class(
            response=json.dumps(data),
            status=200,
            mimetype='application/json')
    return response



@app.route('/perform-multijoin', methods=['POST'])
def perform_multi_join():
    request_json = request.get_json()
    print(request_json)
    app.conn_engine.create_join_lists_and_join(request_json['selectedEdges'], 
                    request_json['writeToFile'], request_json['printop'])
    return "True"



if __name__ == "__main__":
    app.run(debug=True)
