import sqlalchemy as db # Mandatory import for creating database
from join_algorithm import main_join

class EngineCreator():
    """
        A class created to connect to mySQL Server and generate schema graph
        As well as return cursors for individual tables
    """
    def __init__(self,  conn_url='localhost',
                        username = 'root',
                        password='toor'):
        """
            Initialize the connection object. Get Username and password from
            host. At this moment only able to connect to mysql using mysqldb
        """
        self.conn_url = "mysql+mysqldb://{0}:{1}@{2}".format(username, password, conn_url)
        # print(conn_url)
        self.engine = db.create_engine(self.conn_url)
        try:
            self.engine.connect()
        except:
            raise ConnectionError()
        self.inspector = db.inspect(self.engine) ## To get Available Databases


    def get_schemas(self):
        """
            Warning: Donot call this method 
            after an schema has been selected
        """
        table_names = self.inspector.get_schema_names()
        return table_names

    def select_schema(self, schema_name):
        """
            Select a given schema form avaliable list of schemas
            Warning: Exception is not handled here
        """
        assert type(schema_name) == str
        self.engine.dispose() # Dispose the current connection
        self.engine = db.create_engine(self.conn_url + "/" + schema_name)
        self.engine.connect()
        self.inspector = db.inspect(self.engine)
        self.metadata = db.MetaData(bind=self.engine, reflect=True)
        print(self.inspector.get_table_names())
        return self.inspector.get_table_names()

    def create_join_lists_and_join(self, selection, write_to_file = False, print_op = False):
        """
            Based on selection from GUI,
            Create Join Lists Store it.
        """
        join_list_1 = []
        join_list_2 = []
        join_list_3 = []
        if 'rn' in selection:
            region_table = self.metadata.tables['region'].select()
            nation_table = self.metadata.tables['nation_algined2'].select()
            nation_table_cursor = nation_table.execute()
            region_table_cursor = region_table.execute()
            join_list_1.append(region_table_cursor) 
            join_list_1.append(nation_table_cursor)
        if 'nc' in selection:
            customer_table = self.metadata.tables['customer_aligned'].select()
            customer_table_cursor = customer_table.execute()
            join_list_1.append(customer_table_cursor)

        if 'co' in selection:
            orders_table = self.metadata.tables['orders_aligned'].select()
            orders_table_cursor = orders_table.execute()
            join_list_1.append(orders_table_cursor)
        if 'ns' in selection:
            nation_table2 = self.metadata.tables['nation_algined2'].select()
            nation_table2_cursor = nation_table2.execute()
            supplier_table = self.metadata.tables['supplier_aligned'].select()
            supplier_table_cursor = supplier_table.execute()
            join_list_2.append(nation_table2_cursor)
            join_list_2.append(supplier_table_cursor)
        if 'sp' in selection:
            partsupp_table = self.metadata.tables['partsupp_aligned'].select()
            partsupp_table_cursor = partsupp_table.execute()
            join_list_2.append(partsupp_table_cursor)
        if 'sl' in selection:
            supplier_table2 = self.metadata.tables['supplier_aligned'].select()
            supplier_table2_cursor = supplier_table2.execute()
            lineitem_table = self.metadata.tables['lineitem_aligned'].select()
            lineitem_table_cursor = lineitem_table.execute()
            join_list_3.append(supplier_table2_cursor)
            join_list_3.append(lineitem_table_cursor)
        if len(join_list_2) == 0 and len(join_list_3) == 0:
            try:
                print('I am here')
                main_join([join_list_1], write_to_file = write_to_file, print_op = print_op)
            except:
                raise Exception() 


if __name__ == "__main__":
    eg = EngineCreator()
    eg.select_schema('tpc_h_schema')