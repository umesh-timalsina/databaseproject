let connectionForm;
window.addEventListener("load", (event) => {
    connectionForm = document.querySelector("#connectDb");
    if(connectionForm){
    connectionForm.addEventListener('submit', (event) => {
        let hostName = document.querySelector('#hostname').value;
        let userName = document.querySelector('#username').value;
        let password = document.querySelector('#password').value;
        let formData = {
            hostName : hostName,
            userName : userName,
            password : password
        }
        
        postData(url='/connect', data = formData).then(
            (response) => {console.log(response)
                window.location.replace("/mainpage");
            }
        )
        event.preventDefault();
        });
    }
    let schemaSelectionBtn = document.querySelector('#schemaSelectBtn')
    if (schemaSelectionBtn){
        schemaSelectionBtn.addEventListener('click', (event) => {
            let selectedSchema = document.querySelector('#schemas').value;
            data = {
                selectedTable: selectedSchema
            }
            postData(url="/get-tables", data=data).then(
                (response) => {
                // console.log(response); // Log The response and Create Database Graph
                createDatabaseGraph(response.tables); // A utility function based on visjs
                
                //     // Create the list element:
                // var list = document.querySelector('#tablesUl');
                // let array = response.tables
                // for (var i = 0; i < array.length; i++) {
                // // Create the list item:
                // var item = document.createElement('li');
                // item.classList.add('list-group-item')

                // // Set its contents:
                // item.appendChild(document.createTextNode(array[i]));

                // // Add it to the list:
                // list.appendChild(item);
            })


        // })
        // })
        });
    }
    event.preventDefault()
});
let network;
function addEdges(){
    console.log(network.getSelectedNodes()[0])
    network.body.data.edges.add([
        {
            id : 'edge1',
            from : network.getSelectedNodes()[0],
            to : network.getSelectedNodes()[1]
            
        }
    ])
}

function createDatabaseGraph(items){
    // let nodes = []
    // items.forEach((value) => {
    //     nodes.push(
    //         {
    //             id: value,
    //             label: value,
    //             shape: "box"
    //         });
    // });
    // console.log(nodes);
    // var data = { nodes: nodes};
    // var options = {interaction:{
    //   multiselect: true
    // }
    // };
    // var container = document.querySelector('.network');
    // network = new vis.Network(container, data, options);
    var nodes = [
        {
          id:"Region",
          label:"Region",
          shape:"box"
        },
        {
          id:"Nation",
          label:"Nation",
          shape: "box"
        },
        {
          id:"Customer",
          label:"Customer",
          shape: "box"
        },
        {
          id:"Supplier",
          label:"Supplier",
          shape: "box"
        },
        {
          id: "Order",
          label: "Order",
          shape: "box"
        },
        {
          id: "Lineitem",
          label: "Lineitem",
          shape: "box"
        },
        {
          id: "Partsupp",
          label: "Partsupp",
          shape: "box"
        }
        ];
 var edges = [
        {
          from:"Region",
          to:"Nation",
          arrows: "to",
          id: "rn"
        },
        {
          from:"Nation",
          to:"Customer",
          arrows: "to",
          id: "nc"
        },
        {
          from:"Nation",
          to:"Supplier",
          arrows: "to",
          id: "ns"
        },
        {
          from:"Customer",
          to:"Order",
          arrows: "to",
          id: "co"
        },
        {
          from: "Supplier",
          to: "Partsupp",
          arrows: "to",
          id: "sp"
        },
        {
          from:"Supplier",
          to:"Lineitem",
          arrows: "to",
          id: "sl"
        }
 
     ];
     var data = { nodes: nodes, edges: edges};
     var options = {interaction:{
       multiselect: true
     }, 
       layout:{
         randomSeed : 285898
       }
     };
     var container = document.querySelector('.network');
     network = new vis.Network(container, data, options);
}


// Sends Request Back to server with creating trees
function createTree(){
    console.log(network.getSelectedEdges())
    data = {
        selectedEdges : network.getSelectedEdges(),
        writeToFile : document.querySelector("#writetofile").checked,
        printop : document.querySelector("#printop").checked
    }
    postData(url="/perform-multijoin", data=data)



}




function postData(url = ``, data = {}) {
    return fetch(url,  {
        method : "POST",
        mode : "cors",
        cache : "no-cache",
        // cretendials: "same-origin",
        headers : {
            "Content-Type": "application/json; charset=utf-8"
        },
        redirect: "follow", // manual, *follow, error
        referrer: "no-referrer", // no-referrer, *client
        body: JSON.stringify(data), // body data type must match "Content-Type" header
    }).then(response => response.json())
    .catch((error) => console.log(error));
}
    
