# from sql_engine import EngineCreator
# current_engine = EngineCreator()
# current_engine.select_schema('tpc_h_schema')
# region_table = current_engine.metadata.tables['region'].select()
# nation_table = current_engine.metadata.tables['nation_algined2'].select()
# nation_table2 = current_engine.metadata.tables['nation_algined2'].select()
# nation_table3 = current_engine.metadata.tables['nation_algined2'].select()
# customer_table = current_engine.metadata.tables['customer_aligned'].select()
# orders_table = current_engine.metadata.tables['orders_aligned'].select()
# supplier_table = current_engine.metadata.tables['supplier_aligned'].select()
# supplier_table2 = current_engine.metadata.tables['supplier_aligned'].select()
# partsupp_table = current_engine.metadata.tables['partsupp_aligned'].select()
# lineitem_table = current_engine.metadata.tables['lineitem_aligned'].select()
# region_table_cursor = region_table.execute()
# nation_table_cursor = nation_table.execute()
# customer_table_cursor = customer_table.execute()
# orders_table_cursor = orders_table.execute()
# nation_table2_cursor = nation_table2.execute()
# nation_table3_cursor = nation_table3.execute()
# supplier_table_cursor = supplier_table.execute()
# supplier_table_cursor2 = supplier_table2.execute()
# partsupp_table_cursor = partsupp_table.execute()
# lineitem_table_cursor = lineitem_table.execute()


# linear_join_list = [region_table_cursor, nation_table_cursor, customer_table_cursor, orders_table_cursor]
# linear_join_list2 = [nation_table2_cursor, supplier_table_cursor, partsupp_table_cursor]
# linear_join_list3 = [nation_table3_cursor, supplier_table_cursor2, lineitem_table_cursor]


def iterative_join_linear(branch_arr, sid_tuples, print_list, save_to_file=False, print_op=False):
    """
        Perform iterative join of a linear branch using fetchone
        branch_arr: List of Cursors to the tables forming linear branch
        sid_tuples : Another List of tuples containing parent and child sid(Join Attributes)
        print_list : List Used for printing
    """
    fp = open('./op_file.txt', 'w+')
    for i in range(len(branch_arr)):
        print_list.append(branch_arr[i].fetchone())
    print_count = 0
    j = 0
    while True:
        try:
            for i in range(len(print_list)-1):
                if print_list[i][sid_tuples[i][-1]] != print_list[i+1][sid_tuples[i][0]] and i != len(print_list) - 2:
                    print_list[i] = branch_arr[i].fetchone()
                if i == len(print_list) - 2:
                    while(print_list[i][sid_tuples[i][-1]] == print_list[i+1][sid_tuples[i][0]]):
                        if print_op: 
                            print(*print_list)
                        if save_to_file:
                            fp.write(print_list)
                        print_count+=1
                        print_list[i+1] = branch_arr[i+1].fetchone()
                    else:
                        print_list[i] = branch_arr[i].fetchone()

        except:
            print("Complete One Join:", print_count)
            print_count = 0
            break


def iterative_join_linear_v2(branch_arr, sid_tuples, int_buf,
                                print_list, branch_index):
    """
        Perform iterative join of a linear branch using fetchone
        branch_arr: List of Cursors to the tables forming linear branch
        sid_tuples : Another List of tuples containing parent and child sid(Join Attributes)
        print_list : List Used for printing
    """
    for i in range(len(branch_arr)):
        print_list.append(branch_arr[i].fetchone())
    print_count = 0
    j = 0
    while True:
        try:
            for i in range(len(print_list)-1):
                if print_list[i][sid_tuples[i][-1]] != print_list[i+1][sid_tuples[i][0]]:
                    if i == branch_index:
                        return
                    print_list[i] = branch_arr[i].fetchone()
                if i == len(print_list) - 2:
                    while(print_list[i][sid_tuples[i][-1]] == print_list[i+1][sid_tuples[i][0]]):
                        print(*print_list)
                        int_buf.append(print_list.copy())
                        print_count+=1
                        print_list[i+1] = branch_arr[i+1].fetchone()
                    else:
                        if i == branch_index:
                            print(i)
                            return
                        print_list[i] = branch_arr[i].fetchone()
            # break
            j+=1
            # print("Complete One Join:", print_count)
            # print_count = 0
        except:
            # print("Complete One Join:", print_count)
            print_count = 0
            raise Exception()

def main_join(join_lists, write_to_file = False, print_op = False):
    print_list1 = []
    print_list2 = []
    print_list3 = []
    intermedeate_buffer1 = []
    intermedeate_buffer2 = []
    intermedeate_buffer3 = []
    if len(join_lists) == 1:
        # print([(-1, 0)]*len(join_lists[0]))
        iterative_join_linear(join_lists[0], [(0, -1)]*len(join_lists[0]), print_list1, write_to_file, print_op)
    
    if len(join_lists) == 2:
        pass



